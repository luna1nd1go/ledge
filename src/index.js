/**
 * index.js
 * Entry point for the app. Command line args configure run mode (prod, test)
 */

// imports
const commandLineArgs = require('command-line-args')
const express = require('express')

// config
const appConfig = {
    port: 3000
}

// command line args
const cliDef = [
    { name: 'mode', group: [ 'prod', 'dev' ], defaultValue: 'dev' }
]
const options = commandLineArgs(cliDef)

// configure express basic routing
const app = express()
app.get("/", (req, res) => res.send('App running!'))
app.use('/api', require('./api'))
app.use('/ui', require('./ui'))

// start express app
app.listen(appConfig.port, () => console.log(`App running on port ${appConfig.port}!`))