/**
 * Router for UI branch of the app.
 */

const express = require('express')
const router = express.Router()

router.get('/', (req, res) => res.send('UI up!'))

module.exports = router