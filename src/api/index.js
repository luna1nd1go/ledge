/**
 * Router for API branch of the app.
 */

const express = require('express')
const router = express.Router()

router.get('/', (req, res) => { res.send("API up!") })

module.exports = router